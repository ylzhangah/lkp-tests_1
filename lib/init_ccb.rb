#!/usr/bin/env ruby

CCB = "/ccb"
def clone_ccb
  %x(git clone https://gitee.com/duan_pj/ccb.git #{CCB})
  exit(99) unless File.exist?("#{CCB}/sbin/cli/ccb")
  %x(cp -r "#{CCB}/sbin/cli" "#{ENV['LKP_SRC']}/sbin/")
  %x(cp -r "#{CCB}/lib/eulermaker_build.rb" "#{ENV['LKP_SRC']}/lib/")
end
