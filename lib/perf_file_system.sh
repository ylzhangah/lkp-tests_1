#!/bin/sh
. "$LKP_SRC/lib/log.sh"

# 性能测试需要固定测试盘，需要提前在环境上创建对应逻辑卷
# VG_NAME=test
# LV_NAME=performancetest_kongpan
# disk=/dev/disk/by-id/scsi-xxx
# wipefs -a ${disk}
# pvcreate -y ${disk}
# vgcreate -y ${VG_NAME} ${disk}
# lvcreate -y -l 100%FREE -n ${LV_NAME} ${VG_NAME}


VG_NAME=test
LV_NAME=performancetest_kongpan


mk_fs_mount()
{
    local lvm_dir="/dev/${VG_NAME}/${LV_NAME}"
	  local mout_dir=$1
    if [ ! -b "${lvm_dir}" ];then
      log_error "${lvm_dir} is not a block device" && exit 1
    fi

    if [ "${mout_dir}" == "" ];then
        log_error "mk fs lack mount path!" && exit 1
    elif [ ! -d "$1" ];then
        mkdir -p "${mout_dir}"
    fi

    fstype=$(df -T|grep -w /|awk '{printf $2}')
    if [[ "$fstype" =~ ext. ]];then
                    log_cmd mkfs -t "${fstype}" -F "${lvm_dir}"
    elif [ "$fstype" = "xfs" ];then
                    log_cmd mkfs -t "${fstype}" -f "${lvm_dir}"
    fi
    log_cmd mount "${lvm_dir}" "${mout_dir}"
}
