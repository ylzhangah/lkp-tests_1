#!/usr/bin/env ruby

require 'json'
require 'rest-client'

def report_job_step(step)
  resource = RestClient::Resource.new("http://#{ENV['LKP_SERVER']}:#{ENV['LKP_CGI_PORT']}/~#{ENV['LKP_USER']}")
  begin
    resource["cgi-bin/report-job-step?job_id=#{ENV['job_id']}&step=#{step}"].get
  rescue Exception => e
    pp "Failed to report job step: #{e.message}"
  end
end
