#!/bin/bash

add_repo_debian()
{
        oldIFS=$IFS
        IFS=" "
        set -- $mount_repo_addr
        IFS=$oldIFS
        for zMountRepoName in $mount_repo_name
        do
                repo_file_name=$(echo "$zMountRepoName" | tr '/' '-')
                baseurl=$1
                cat <<-EOF >> /etc/apt/sources.list.d/"${repo_file_name}.list"
                deb $baseurl ${repo_file_name} main contrib non-free
                deb-src $baseurl ${repo_file_name} main contrib non-free
                EOF
                shift
        done
}
