#version=DEVEL
# Use text mode install
text


%packages
@^minimal-environment

%end

# System language
lang en_US.UTF-8

# Network information
network  --hostname=localhost.localdomain

# Use network installation
url --url="http://172.168.131.2:20009/os/install/iso-content/openeuler/x86_64/20.09/"

# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"

reboot --eject

ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
autopart
# Partition clearing information
clearpart --all --initlabel --drives=vda

# System timezone
timezone Asia/Shanghai --utc

# Root password
rootpw --iscrypted $6$PNsXfx01cyaHHeMA$jNsie3JG4d3rd4pbWa3nJjF6mdG8OafLjcqs3u5DMCAzdO6hntf58OzLxo1AmmDv6dRt0t4Vh.wiEC05LJy5k/

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=8 --minquality=1 --strict --nochanges --notempty
pwpolicy user --minlen=8 --minquality=1 --strict --nochanges --emptyok
pwpolicy luks --minlen=8 --minquality=1 --strict --nochanges --notempty
%end
