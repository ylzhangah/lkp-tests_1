#version=DEVEL
# Use text mode install
text


%packages
@^minimal-environment
@standard

%end

# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=enp3s0 --onboot=off --ipv6=auto --activate
network  --hostname=localhost.localdomain

# Use CDROM installation media
cdrom

# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"

ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
autopart
# Partition clearing information
clearpart --all --initlabel --drives=vda

# System timezone
timezone Asia/Shanghai --utc

reboot --eject

# Root password
rootpw --iscrypted $6$/qKHpGryfTqHMoo/$JCMcgxFqienzLbHxYiB0dcmczGKr.Mv0gZVoFxlsz1mUuBJXyEaFxDXk79qNhB92cqNgEC6rHL92w7QVxpTiq.

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=8 --minquality=1 --strict --nochanges --notempty
pwpolicy user --minlen=8 --minquality=1 --strict --nochanges --emptyok
pwpolicy luks --minlen=8 --minquality=1 --strict --nochanges --notempty
%end
