#version=DEVEL
# Use text mode install
text


%packages
@^minimal-environment

%end

# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=enp3s0 --onboot=off --ipv6=auto --no-activate
network  --hostname=localhost.localdomain

# Use hard drive installation media
harddrive --dir= --partition=LABEL=openEuler-20.03-LTS-SP3-aarch64

# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"

ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
autopart
# Partition clearing information
clearpart --all --initlabel --drives=vda

# System timezone
timezone Asia/Shanghai --utc

# Root password
rootpw --iscrypted $6$pu07zeNXnl3ZgTlj$YdqRvNRMcc6.jGkd..XG1XgXNlJqa7QPS.Qo2ohlpV/NjGMwwykF9D3eE5f9MHbDQ/Mx2f0QiVQxeO4YahwwA/

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=8 --minquality=1 --strict --nochanges --notempty
pwpolicy user --minlen=8 --minquality=1 --strict --nochanges --emptyok
pwpolicy luks --minlen=8 --minquality=1 --strict --nochanges --notempty
%end
